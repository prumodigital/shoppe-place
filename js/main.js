$(document).ready(function(){
	f();
	updateCart(CART);
});
var urlCartZipCode = '/cart/';
if(zipCode != ""){
	urlCartZipCode = urlCartZipCode+'?zipCode='+zipCode;
}	
new WOW().init();
var a = [],
t = new Set([]);
"use strict";
var c =
		'<div class="item"><div class="row"><div class="col-sm-2"><img src="{PRODUCT_IMAGE}" alt="" class="pull-left" /></div><div class="col-sm-7"><p>{VARIANT_NAME}</p></div><div class="col-sm-3 col-price"><p class="variant-price"><strong>{VARIANT_PRICE}</strong></p><p class="variant-qty">{VARIANT_QUANTITY_PRESENTATION}</p></div></div></div>',
	d = '<div class="row"><div class="col-md-12 text-right"><span class="cart-sumary-total">Descontos: {CART_DISCOUNT_TOTAL}</span></div></div>',
	m =
		'<div class="row cart-sumary-gtc"><div class="col-md-12 text-right">' +
		(SHOW_PRODUCT_PRICE ? '<span class="cart-sumary-total">TOTAL: {CART_TOTAL}</span>' : "") +
		'<a href="'+urlCartZipCode+'" class=" pull-right btn-blue btn-cart-sumary"><i class="material-icons-outlined">shopping_bag</i> &nbsp;&nbsp;Ir para o carrinho</a></div></div>';


$("body").on("cart-success", [".flexy-add-to-cart", ".btn-add-to-cart-qty"], function (e, t) {
	var o;
	updateCart(t),
		(t = 4e3),
		(o = $(".cart-sumary .header-mini-cart")).addClass("open"),
		window.setTimeout(function () {
			o.removeClass("open");
		}, t),
		(window.innerWidth < 991 || screen.width < 991) && alert("Produto adicionado ao carrinho!");
});
$("body").on("cart-error", function (e, t) {
	alert(t);
});
/*
$("body").on("cart-success", [".flexy-add-to-cart", ".btn-add-to-cart-qty"], function (e, t) {
	var o = 4e3;
	var a = $(".cart-sumary .header-mini-cart");
	updateCart(t);
	if($(window).width()>991){
		a.addClass("open");
		window.setTimeout(function () {	a.removeClass("open"); }, o)
	}
	else {
		alert("Produto adicionado ao carrinho!");	
	}
}),
$("body").on("cart-error", function (e, t) {
	alert(t);
});
*/
$("body").find(".holder").length || $("body").append("<div class='holder'></div>");
$("div.holder").jPages({ containerID: "products", previous: ".feature-block a[data-role='prev']", next: ".feature-block a[data-role='next']", animation: "fadeInRight", perPage: 4 });

function updateCart(e) {
	var t = SHOW_PRODUCT_PRICE ? $.number(e.totalWithDiscount, 2, ",", ".") : "",
		o = $(".shopping-cart-content").empty(),
		a = (function (e) {
			var t = [];
			if ((0 < e.items.length && (t = $.merge(t, e.items)), e.sales && 0 < e.sales.length)) for (var o = 0; o < e.sales.length; o++) t = $.merge(t, e.sales[o].items);
			return t;
		})(e);
				
	if (e && 0 != a.length) {
		o.append(m.replace("{CART_TOTAL}", "R$ " + t));
		for (var i = 0, s = 0; s < a.length; s++) {
			var n = a[s],
				r = SHOW_PRODUCT_PRICE ? "R$ " + $.number(n.totalPriceWithDiscount, 2, ",", ".") : "",
				l = n.quantity;
			n.variant.isFractionalStock && (l = 1),
				o.append(
					c
						.replace("{PRODUCT_IMAGE}", n.image ? n.image : "/bundles/flexyftwostore/img/product-placeholder.gif")
						.replace("{VARIANT_NAME}", n.name)
						.replace("{VARIANT_PRICE}", r)
						.replace("{VARIANT_QUANTITY_PRESENTATION}", "Qtd: " + g(n.quantity))
				),
				(i += l);
		}
		0 < e.salePromotionDiscountTotal && SHOW_PRODUCT_PRICE && o.append(d.replace("{CART_DISCOUNT_TOTAL}", "R$ " + $.number(e.salePromotionDiscountTotal, 2, ",", "."))),
			window.innerWidth < 991 || screen.width < 991
				? $(".cart-items-count").html('<span class="badge badge-danger">' + Number(i) + "</span>")
				: $(".cart-items-count").html(Number(i));
	} else o.html("<h4>Nenhum produto em seu carrinho.</h4>");
}

function g(e) {
	return -1 < (e = e.toString()).indexOf(".") && (e = (e = Number(e).toFixed(3)).replace(".", ",")), e;
}

function f() {
	if ($("[name='customer[type]']").length) {
		var e = $("[name='customer[type]']").val(),
			t = $("#customer_customerType"),
			o = t.find("option");
		"cpf" == e
			? ($(".cpf").show().find(":input").attr("required", "required"),
			  $(".cnpj").hide().find(":input").removeAttr("required"),
			  t.val(
				  o.filter(function () {return "B2C" == $(this).text();}).val()
			  ))
			: ($(".cnpj").show().find(":input:not(#check):not(#customer_taxationRegime)").attr("required", "required"),
			  $(".cpf").hide().find(":input").removeAttr("required"),
			  t.val(
				  o.filter(function () { return "B2B" == $(this).text();}).val()
			  ));
	}
}

function h(e) {
	return (e = e.replace(/\./g, "")), parseFloat(e.replace(/,/g, "."));
}

function s(e, t) {
	return !(t && t < e) || (alert("Não foi possível adicionar " + e + " produtos ao carrinho."), !1); // + " Atualmente possuímos apenas " + t
}
	
// search controls
function e() {
	$(".search-box").animate({ opacity: "toggle" }, 500, function () {}), $(".search-modal-background").animate({ opacity: "toggle" }, 500, function () {});
}
$(".search-box-toggle").click(function () {
	e(), $(".search-input").focus();
});
$(".search-modal-background").click(function () {
	e();
});

//general
$(".time").countdown({ date: new Date(new Date().getTime() + 864e5), yearsAndMonths: !0, leadingZero: !0 });
$(".ul-side-category li a").click(function () {
	var e = $(this).next();
	return !e.hasClass("sub-category") || ("none" === e.css("display") ? $(this).next().slideDown() : ($(this).next().slideUp(), $(this).next().find(".sub-category").slideUp()), !1);
});
$("[name='customer[type]']").on("change blur", function () {
	f();
});
$("#check").on("change click", function () {
	$(this).is(":checked") ? $("#customer_cnpj_stateRegistration").addClass("readonly").attr("readonly", "readonly").val("ISENTO") : $("#customer_cnpj_stateRegistration").removeClass("readonly").removeAttr("readonly").val("");
});
$(".main-category-block label").on("click", function () {
	window.location.href = $(this).data("url");
});
$("#partners img").on("mouseenter", function (e) {
	var t = $(this);
	t.data("src", t.attr("src")).attr("src", t.data("hover"));
}).on("mouseleave", function (e) {
	var t = $(this);
	t.attr("src", t.data("src"));
});
$(".flexy-cart-product-quantity, .input-number-inf")
	.on("focus", function (e) {
		var t = $(this);
		t.data("quantity", h(t.val()));
	})
	.on("blur", function (e) {
		var t = $(this);
		t.val() && t.val() != t.data("quantity") ? t.trigger("change") : t.val(t.data("quantity"));
	});

$(".tool_tip").tooltip();
$("#product-zoom").elevateZoom({ zoomType: "inner", cursor: "crosshair", zoomWindowFadeIn: 500, zoomWindowFadeOut: 750 });
$(".stock-verified").on("change keypress keyup keydown", function (e) {
	var t = $(this);
	s(Number(t.val()), t.data("stock")) || (t.val(g(t.data("stock"))).trigger("change"), e.preventDefault(), e.stopImmediatePropagation());
});
var w,
b = $(".flexy-weight-mask");
b.mask("#######0,000", { placeholder: "0,000", reverse: !0 });
b.on("keyup", function (i) {
	clearTimeout(w),
		(w = setTimeout(function () {
			var e, t, o, a;
			(e = i), (t = $(this).val()), (o = $(this)), (a = o.data("stock")), (s(h(t), a) || (o.val(g(a)).trigger("change"), e.preventDefault(), e.stopImmediatePropagation(), 0)) && $(this).trigger("blur");
		}, 1200));
}),
b.on("keydown", function (e) {
	clearTimeout(w);
})
		
//home
var t1 = 0,
	o = $("#header"),
	a = $("#header-main-fixed"),
	n = $(".header-bg"),
	r = $(".top-header-bg"),
	l = document.getElementById("gotop"),
	u = null != l;
$(window).scroll(function () {
	var e = $(this).scrollTop();
	t1 < e
		? 50 < e && (o.addClass("header-top-fixed"), o.find(".header-top-row").addClass("dis-n"), n.addClass("header-bg-fixed"), a.addClass("header-main-fixed"), r.addClass("top-header-bg-fix"), u && (l.style.display = "block"))
		: e < 50 &&
		  (o.removeClass("header-top-fixed"),
		  o.find(".header-top-row").removeClass("dis-n"),
		  a.removeClass("header-main-fixed"),
		  n.removeClass("header-bg-fixed"),
		  r.removeClass("top-header-bg-fix"),
		  u && (l.style.display = "none")),
		(t1 = e);
}),
$(".dropdown").hover(
	function () {
		$(this).addClass("open");
	},
	function () {
		$(this).removeClass("open");
	}
);
$('#selectCustomer').select2({
	placeholder: ' - selecione um cliente -',
}).change(function (e) {
			e.preventDefault();
			var selectedData = $(this).val();
});
$("#selectCustomer").change(function() {
	var customerId = $(this).val();

	if (customerId == "new") {
		window.location.href = urlCustomerCreate;
		return;
	}

	$.ajax({
		url : urlCustomerChange,
		data : {
			customerId : customerId
		},
		success : function() {
			window.location.reload();
		},
		error : function() {
			alert("{{ 'store.customerSelectError'|trans }}");
		}
	});
});
//owl carousel
$(".product-related-nav .next").click(function () {
	$($(this).data("carousel")).trigger("owl.next");
});
$(".product-related-nav .prev").click(function () {
	$($(this).data("carousel")).trigger("owl.prev");
});
$(".top-page").click(function () {
	(document.body.scrollTop = 0), (document.documentElement.scrollTop = 0);
}),
$("#nav-tabs .next").click(function () {
	$("#owl-new").trigger("owl.next"), $("#owl-featured").trigger("owl.next");
}),
$("#nav-tabs .prev").click(function () {
	$("#owl-new").trigger("owl.prev"), $("#owl-featured").trigger("owl.prev");
}),
$("#nav-tabs2 .next").click(function () {
	$("#owl-new2").trigger("owl.next"), $("#owl-featured2").trigger("owl.next");
}),
$("#nav-tabs2 .prev").click(function () {
	$("#owl-new2").trigger("owl.prev"), $("#owl-featured2").trigger("owl.prev");
}),
$('.home-product-carousel').owlCarousel({
	items: 4,
	itemsCustom: !1,
	itemsDesktop: [1199, 4],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 2],
	itemsTabletSmall: !1,
	itemsMobile: [650, 1],
	singleItem: !1,
	itemsScaleUp: !1,
	responsive: !0,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,
	autoPlay: !1,
	stopOnHover: !1,
	navigation: true,
});
$('.home-product-carousel-seller').owlCarousel({
	items: 3,
	itemsCustom: !1,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 2],
	itemsTablet: [768, 2],
	itemsTabletSmall: !1,
	itemsMobile: [650, 1],
	singleItem: !1,
	itemsScaleUp: !1,
	responsive: !0,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,
	autoPlay: !1,
	stopOnHover: !1,
	navigation: true,
});
$('.product-related-carousel').owlCarousel({
	items: 4,
	itemsCustom: !1,
	itemsDesktop: [1199, 4],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 2],
	itemsTabletSmall: !1,
	itemsMobile: [650, 1],
	singleItem: !1,
	itemsScaleUp: !1,
	responsive: !0,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,
	autoPlay: !1,
	stopOnHover: !1,
	navigation: true,
});

//navigation
function filterToggle(){
	$(".sidebar-menu").animate({
		opacity: "toggle"
	}, 500, function() {
		// Animation complete.
	});

	$(".filter-menu-modal-close").animate({
		opacity: "toggle"
	}, 500, function() {
		// Animation complete.
	});
}

$( ".filter-menu-btn" ).click(function() {
	$("html").addClass("mobile-html-fix");
	$("body").addClass("mobile-body-fix");
	$(".top-page").css("display", "block");
	filterToggle();
});

$( ".filter-menu-modal-close" ).click(function() {
	$("html").removeClass("mobile-html-fix");
	$("body").removeClass("mobile-body-fix");
	$(".top-page").css("display", "none");
	filterToggle();
});
/*
$(window).resize(function() {
	if($(window).width() > 992){
		$(".sidebar-menu").css("display", "block");
		$(".filter-menu-modal-close").css("display", "none");
	}else{
		$(".sidebar-menu").css("display", "none");
		$(".filter-menu-modal-close").css("display", "none");
	}
});
*/
$('.flexy-list-btn').click(function(event){
	event.preventDefault();
	$('.flexy-display-mode').addClass('flexy-list-display');
});

$('.flexy-grid-btn').click(function(event){
	event.preventDefault();
	$('.flexy-display-mode').removeClass('flexy-list-display');
});

//cart and freight
	
$(".additional-add-to-cart").on("click", function () {
	var e,
		t,
		o = $(this).data("variant-id");
	-1 < i(o) ? ((t = o), a.splice(i(t), 1), alert("Produto removido!")) : ((e = o), a.push(e), alert("Produto adicionado!")), $(".main_cart_add").data("additional-variant-ids", a);
});

$(".coupon-validate").on("keyup", function (e) {
	$(this).val(
		$(this)
			.val()
			.replace(/[^0-9a-zA-Z_]/g, "")
	);
});
		
$("body").on("click", ".btn-cep", function(event){
	if (-1 !== window.location.pathname.indexOf("quote")) {
		var e = $(".flexy-input-postcode").val(),
			t = [location.protocol, "//", location.host, location.pathname].join("") + "?zipCode=" + e,
			o = "";
		$(".f2-quote-observation").each(function () {
			var e = $(this).data("observation"),
				t = $(this).val();
			o += "&" + e + "=" + t;
		}),
		(window.location = t + o);
	}
});

$("body").on("click", ".additional-to-cart", function(event) {
	var e = $(this).data("addition-id");
	t.add(e),
		$(".flexy-add-to-cart").one("cart-error", function () {
			t.delete(e);
		});
});
$("body").on("click", ".btn-qty button.remove", function(event) {
	let inputQty = $( this ).parents('.btn-qty').find('.product-quantity');
	let qty = parseFloat($(inputQty).val());
	if (qty > 1) {
		$(inputQty).val(qty - 1);
	}
});
$("body").on("click", ".btn-qty button.add", function(event) {
	let inputQty = $( this ).parents('.btn-qty').find('.product-quantity');
	let qty = parseFloat($(inputQty).val());
	$(inputQty).val(qty + 1);
});

$("body").on("click", ".btn-add-to-cart-qty", function(event) {
	var $this = $(this);
	var inputQty = $this.parents('.controls-add-cart').find('.product-quantity');
	var qty = parseFloat($(inputQty).val());
	var additionalIds = $this.data('additional-variant-ids') || [];
	var dc = $this.data('distributioncenter') || [];
	var varId = $this.data('variant-id');
	var attrVal = {};

	$.ajax({
		url: "/cart/add",
		type: "post",
		dataType: "json",
		data: {
			id: varId,
			quantity: qty,
			additionalVariantId: additionalIds,
			distributionCenter: dc,
			attributeValues: attrVal
		},
		beforeSend: function() {
			$this.parents('.controls-add-cart').find('.btn-add-to-cart-qty').attr("disabled", !0);
		},
		success: function(e) {
			$this.parents('.controls-add-cart').find('.btn-add-to-cart-qty').trigger("cart-success", [e.cart]);
			//"quote" === t && (window.location = "/quote/")
		},
		error: function(e) {
			alert(e.responseJSON.error);
			$(inputQty).val(qty);
		}
	}).always(function() {
		$this.parents('.controls-add-cart').find('.btn-add-to-cart-qty').removeAttr("disabled");
	});
});

$("body").on("click", ".card_product_quantity button.remove", function(event) {
	let inputQty = $( this ).parents('.card_product_quantity').find('.product-quantity');
	let qty = parseFloat($(inputQty).val());
	if (qty > 1) {
		$(inputQty).val(qty - 1);
	}
	$(inputQty).blur();
});

$("body").on("click", ".card_product_quantity button.add", function(event) {
	let inputQty = $( this ).parents('.card_product_quantity').find('.product-quantity');
	let qty = parseFloat($(inputQty).val());
	$(inputQty).val(qty + 1);
	$(inputQty).blur();
});

function grava_cookie(key, value){
	Cookies.set(key, value, { expires: 365 });
}

function resetPaymentFlags(){
	$('.nav-credit-flags').find("li").each(function() {  
		var imgLi = $(this).find('img'); 
		$(imgLi).css('opacity', '1');
	})
}
$(".nav-credit-flags").on("click", function (){
	$('.nav-credit-flags').find("li").each(function() {  
		var radioLi = $(this).find('input[type=radio]'); 
		var imgLi = $(this).find('img'); 
		if($(radioLi).is(":checked")) {
			$(imgLi).css('opacity', '1');
		}
		else { 
			$(imgLi).css('opacity', '0.1');
		} 
	})
	$('.credit-card-fields').show();
});

$('#payment-box').click(function(e) {  
	if($(".payment-box").hasClass("disabled")){
		alert("Selecione um endereço e uma forma de entrega!");
	}
});

$("#customer_defaultShippingAddress_postcode").blur(function(e){
	var cep = $(this).val();
	$.ajax({
		type: 'GET',                          
		url: "https://viacep.com.br/ws/"+cep+"/json/",                            
		dataType: "JSON",                                                         
		success: (resp) =>{
			$("#customer_defaultShippingAddress_city").val(resp.localidade);
			$("#customer_defaultShippingAddress_state").val(resp.uf);
			$("#customer_defaultShippingAddress_district").val(resp.bairro);
			$("#customer_defaultShippingAddress_street").val(resp.logradouro);
		},
		error: (resp) =>{
			console.log(resp);                    
		}                            
	});
});

$("#address_postcode").blur(function(e){
	var cep = $(this).val();
	$.ajax({
		type: 'GET',                          
		url: "https://viacep.com.br/ws/"+cep+"/json/",                            
		dataType: "JSON",                                                         
		success: (resp) =>{
			$("#address_city").val(resp.localidade);
			$("#address_state").val(resp.uf);
			$("#address_district").val(resp.bairro);
			$("#address_street").val(resp.logradouro);
		},
		error: (resp) =>{
			console.log(resp);                    
		}                            
	});
});

$(document).on('click', '.flexy-accept-cookie', function(){
	Cookies.set('agree_cookies', 'accept', { expires: 365 });
	Cookies.set('prm_dtd', new Date(), { expires: 365 });
	$('.lgpd').hide();
});

function cookie_agree(){
	var cook = Cookies.get('agree_cookies');
	$('.cookie_test').html(cook);
	if (cook != "accept"){
		console.log('Cookie: '+cook);
		$('.lgpd').show();
	}
}

$('.f2-multiple-dc-shipping-estimate-action-2').on('click', function (e) {
	e.preventDefault();
	grava_cookie('zipCode', $('.f2-shipping-estimate-postcode').val());
	shippingCalculation()
});

function shippingCalculation() {
	var postcode = $('.f2-shipping-estimate-postcode').val();
	var container = $('.f2-shipping-estimate-container');
	var variantId = $('.f2-shipping-estimate-variant-id').val();

	if (variantId == undefined) {
		alert('Informe o produto');
		return;
	}

	if (postcode == undefined || postcode === "") {
		alert('Informe o CEP');
		return;
	}

	$("input[name='distributionCenterReferenceCode']").each(function() {
		var dcRefCode = $(this).val();
		$('#shipping-'+dcRefCode).show();
		$.calculate_shipping_by_variant(
			'html',
			function (data) {
				$('#shipping-'+dcRefCode).html('<div class="estimate-spinner"><i class="fa fa-spinner fa-spin"></i> Calculando frete...</div>');
			},
			function (data) {
				$('#shipping-'+dcRefCode).html(data);
				container.html('');
			},
			postcode,
			$('#quantity_'+dcRefCode).val(),
			variantId,
			dcRefCode
		);
	});
}

$('#zipCode').keyup(function(e){
	if(e.keyCode == 13) {
		grava_cookie('zipCode', $('.f2-shipping-estimate-postcode').val());
		shippingCalculation();
	}
});

// $("button.account-toggle").on("click", function (e){
// 	var cd = $('.welcome .customer-data');
// 	if($(cd).is(":visible")){
// 		$(cd).hide();
// 	}
// 	else{
// 		$(cd).show();
// 	}
// });

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})